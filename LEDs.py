# File: LEDs.py
# Author: Adam Fuller
# Date: 4/6/2017
# Description: A Python library for controlling the 4 user (USR)
# LEDs on the Beaglebone Black.

import time

# The common prefix of all LEDs' directories.
sbase = '/sys/class/leds/beaglebone:green:usr'

def _settrigger(i, trigger):
	assert trigger in ['none', 'nand-disk', 'mmc0', 'mmc1', 'timer',
		'oneshot', 'heartbeat', 'backlight', 'gpio', 'cpu0',
		'default-on', 'transient']

	with open(sbase+str(i)+'/trigger', 'w') as f:
		f.write(trigger)
		f.flush()

def shot(i):

	"""
	Blink LED once.

	Make sure trigger of LED i is set to 'oneshot' first.
	"""

	_settrigger(i, 'oneshot')

	with open(sbase+str(i)+'/shot', 'w') as f:
		f.write(" ")
		f.flush()

def on(i):

	"""
	Switch LED on.
	"""

	with open(sbase+str(i)+'/brightness', 'w') as f:
		f.write("1")
		f.flush()

	_settrigger(i, 'none')

def off(i):

	"""
	Switch LED off.
	"""

	with open(sbase+str(i)+'/brightness', 'w') as f:
		f.write("0")
		f.flush()

	_settrigger(i, 'none')

def dim(i, frac, t=4):

	"""
	Set the LED brightness as a fraction of full power.
	
	i : int
		LED identifier.
	
	frac : float
		1.0 is full brightness; 0 is off.

	t : int
		Cycle time [ms].

	"""

	assert 0 <= frac <= 1.0

	ton = int(t*frac)
	toff = int(t*(1-frac))

	# This is a backup, as sometimes setting frac=0 doesn't switch
	# the LED off, even though delay_on is set to zero.
	if ton == 0: # No on-time.
		off(i)
	elif frac == 1: # Shorthand for "full noise".
		on(i)
	else: # Otherwise, dim.

		_settrigger(i, 'timer')

		with open(sbase+str(i)+'/delay_on', 'w') as f:
			f.write(str(ton))
			f.flush()
		
		with open(sbase+str(i)+'/delay_off', 'w') as f:
			f.write(str(toff))
			f.flush()

def blink(i, n, ton=0.020, toff=0.020):

	"""
	Blink LED i n times.
	"""
	for bob in range(n):
		on(i)
		time.sleep(ton)
		off(i)
		time.sleep(toff)

class LED():

	def __init__(self, i, trigger='none', t=4):

		self.i = i
		self.trigger = trigger
		self.t = t

	def _settrigger(self, trigger):
		self.trigger = trigger
		_settrigger(self.i, trigger)
	def shot(self):
		shot(self.i)
	def on(self):
		on(self.i)
	def off(self):
		off(self.i)
	def dim(self, frac):
		dim(self.i, frac, self.t)
	def blink(self, n, ton=0.020, toff=0.020):
		blink(self.i, n, ton, toff)

def wave(nLEDs=4, n=1, t=0.1, dir=1):

	"""
	Display a "wave" pattern on the LEDs.

	nLEDs : int
		How many LEDs to use?

	dir : int
		Direction to wave: -1 is in order of descending LED number, 1
		is ascending, 0 goes back and forth.

	"""

	import itertools as it

	if dir == 1:
		ll = it.cycle(range(nLEDs))
	elif dir == -1:
		ll = it.cycle(range(nLEDs-1, -1, -1))
	elif dir == 0:
		ll = it.cycle(range(nLEDs)+range(nLEDs-2,0,-1))

	if dir in [1,-1]:
		clicks = n*nLEDs
	else:
		clicks = (2*nLEDs-2)*n+1

	for i in range(clicks):
		x = ll.next()
		on(x)
		time.sleep(t)
		off(x)

def pattern(pat, t=20):

	"""
	Display an arbitrary pattern on the LEDs.  No checks of any kind
	are done.

	pat : tuple
		A tuple of LED values.

	"""

	for e,v in enumerate(pat):
		dim(e, v, t=t)

def animate(lpat, t=0.1):

	"""
	Display a sequence of arbitrary patterns on the LEDs.

	lpat : list
		A list of patterns.  Each pattern must be a tuple containing
		the desired LED values for each frame of the animation.

	"""

	for pat in lpat:
		pattern(pat)
		time.sleep(t)
